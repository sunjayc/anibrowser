#!/usr/bin/env python3

from getch import getch
from grabber import *
from prettyprint import *
from operator import attrgetter
import os

def printSeiyuu(id):
	seiyuu = Seiyuu.seiyuus[id]
	entries = []
	title = seiyuu.name + ' (' + seiyuu.lang + ')\n\nCharacters:\n'

	characters = seiyuu.characters
	characters.sort(key=attrgetter('name'))
	for char in characters:
		intro = char.name + ' from '
		animes = list(char.animes)
		animes.sort(key=attrgetter('name'))
		entry = intro + animes[0].name + ' (' + char.animes[animes[0]] + ')\n'
		for i in range(1,len(animes)):
			entry += ' ' * len(intro) + animes[i].name + ' (' + char.animes[animes[i]] + ')\n'
		entries.append(entry)

	charsel = 0
	def choose():
		nonlocal charsel
		charsel = ppmenu(entries, title, charsel)
		return charsel

	while not choose() == -1:
		printCharacter(characters[charsel].id)

def printCharacter(id):
	char = Character.characters[id]
	titles = [(0,char.name + '\n\nPlayed by:\n')]
	entries = []
	for sei in char.seiyuus:
		entry = '  ' + sei.name + '\n'
		entries.append(entry)
	nseiyuu = len(entries)
	#entry = entries.pop()
	#entry += '\n\b\bCharacter in:\n'
	#entries.append(entry)
	titles.append( (nseiyuu, '\nCharacter in:\n') )
	animes = list(char.animes)
	for anime in animes:
		entry = '  ' + anime.name + '\n'
		entries.append(entry)

	entrysel = 0
	def choose():
		nonlocal entrysel
		entrysel = ppmenu(entries, titles, entrysel)
		return entrysel

	while not choose() == -1:
		if entrysel < nseiyuu:
			printSeiyuu(char.seiyuus[entrysel].id)
		else:
			printAnime(animes[entrysel - nseiyuu].id)

def printAnime(id):
	anime = Anime.animes[id]
	title = anime.name + '\n\nCharacters:\n'
	entries = []
	for char in anime.characters:
		if len(char.seiyuus) > 0:
			intro = char.name + ' (' + char.animes[anime] + ') played by '
			entry = intro + char.seiyuus[0].name + '\n'
			for i in range(1,len(char.seiyuus)):
				entry += ' ' * len(intro) + char.seiyuus[i].name + '\n'
		else:
			entry = '  ' + char.name + ' (' + char.animes[anime] + ')\n'
		entries.append(entry)

	charsel = 0
	def choose():
		nonlocal charsel
		charsel = ppmenu(entries, title, charsel)
		return charsel

	while not choose() == -1:
		printCharacter(anime.characters[charsel].id)

@prettyprinter
def main():
	loadData('whole')
	chars = list(Character.characters.values())
	chars.sort(key=attrgetter('name'))
	entries = []
	for char in chars:
		entries.append(char.name + '\n')
	charsel = 0
	def choose():
		nonlocal charsel
		charsel = ppmenu(entries, startat=charsel, typer=True)
		return charsel
	while not choose() == -1:
		printCharacter(chars[charsel].id)
	ppclear() # This should be unnecessary once _prettyprint_win is implemented to create a secondary console handle

main()
