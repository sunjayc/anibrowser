import re
import platform
from functools import wraps
from termsize import getTerminalSize as gettermsize

current_os = platform.system()


if current_os == 'Windows':
	from ctypes import windll, Structure, Union, byref, c_short, c_ulong, c_int, c_ushort, c_wchar, c_char
	winapi = windll.kernel32
	SHORT = c_short
	DWORD = c_ulong
	BOOL = c_int
	WORD = c_ushort
	WCHAR = c_wchar
	CHAR = c_char
	STD_OUTPUT_HANDLE = 4294967285
	NULL = 0
	class COORD(Structure):
		_fields_ = [
			("X", SHORT),
			("Y", SHORT)]
	class CONSOLE_CURSOR_INFO(Structure):
		_fields_ = [
			("dwSize", DWORD),
			("bVisible", BOOL)]
	class SMALL_RECT(Structure):
		_fields_ = [
			("Left", SHORT),
			("Top", SHORT),
			("Right", SHORT),
			("Bottom", SHORT)]
	class CHAR_INFO(Structure):
		class _CHAR_U(Union):
			_fields_ = [
				("UnicodeChar", WCHAR),
				("AsciiChar", CHAR)]
		_fields_ = [
			("Char", _CHAR_U),
			("Attributes", WORD)]


def _prettyprinter_nix(f):
	@wraps(f)
	def wrapper(*args, **kwds):
		def run(_stdscr):
			global stdscr
			stdscr = _stdscr
			stdscr.idlok(1)
			stdscr.scrollok(1)
			curses.curs_set(0)
			f(*args, **kwds)
		curses.wrapper(run)
	return wrapper

def _ppwrite_nix(x, y, msg):
	stdscr.addstr(y, x, msg)

def _ppscroll_nix(top, bottom, n):
	stdscr.setscrreg(top, bottom)
	stdscr.scroll(n)

def _ppgetch_nix():
	return stdscr.getch()

def _ppclear_nix():
	stdscr.clear()


def _prettyprinter_win(f):
	@wraps(f)
	def wrapper(*args, **kwds):
		global hScr
		hScr = winapi.GetStdHandle(STD_OUTPUT_HANDLE)
		# Ideally, we should create a new handle so as not to trash the old one
		cciInfoOld = CONSOLE_CURSOR_INFO()
		cciInfo = CONSOLE_CURSOR_INFO()
		cciInfo.bVisible = False
		cciInfo.dwSize = 10 # Not sure why, but setting this to zero doesn't work
		winapi.GetConsoleCursorInfo(hScr, byref(cciInfoOld))
		winapi.SetConsoleCursorInfo(hScr, byref(cciInfo))
		f(*args, **kwds)
		winapi.SetConsoleCursorInfo(hScr, byref(cciInfoOld))
	return wrapper

def _ppwrite_win(x, y, msg):
	coord = COORD()
	coord.X = x
	coord.Y = y
	lnum = c_ulong()
	for line in msg.split('\n'):
		winapi.WriteConsoleOutputCharacterW(hScr, line, len(line), coord, byref(lnum))
		coord.Y += 1

def _ppscroll_win(top, bottom, n):
	(width, height) = gettermsize()
	sm = SMALL_RECT()
	sm.Top = top
	sm.Bottom = bottom
	sm.Left = 0
	sm.Right = width
	dest = COORD()
	dest.X = 0
	dest.Y = top - n
	cInfo = CHAR_INFO()
	cInfo.Attributes = 7
	cInfo.Char.AsciiChar = 0x20
	winapi.ScrollConsoleScreenBufferA(hScr, byref(sm), byref(sm), dest, byref(cInfo))

def _ppclear_win():
	(width, height) = gettermsize()
	coord = COORD()
	coord.X = 0
	coord.Y = 0
	lnum = c_ulong()
	winapi.FillConsoleOutputCharacterA(hScr, 0x20, width * height, coord, byref(lnum))

def _ppgetch_win():
	from getch import getch as _getch
	c = ord(_getch())
	if c == 224:
		return (c,ord(_getch()))
	return c


__all__ = ['prettyprinter', 'ppwrite', 'ppscroll', 'ppgetch', 'ppmenu', 'ppclear']

if current_os == 'Windows':
	ENTER = 13
	ESC = 27
	BKSP = 8
	UP = (224,72)
	DOWN = (224,80)
	LEFT = (224,75)
	RIGHT = (224,77)
	prettyprinter = _prettyprinter_win
	ppwrite = _ppwrite_win
	ppscroll = _ppscroll_win
	ppclear = _ppclear_win
	ppgetch = _ppgetch_win
else:
	ENTER = 10
	ESC = 27
	BKSP = 263
	UP = 259
	DOWN = 258
	import curses
	prettyprinter = _prettyprinter_nix
	ppwrite = _ppwrite_nix
	ppscroll = _ppscroll_nix
	ppgetch = _ppgetch_nix
	ppclear = _ppclear_nix


def linecount(string):
	if string == '':
		return 0
	lines = string.split('\n')
	(width, height) = gettermsize()
	count = len(lines)
	for line in lines:
		count += (max(len(line) - 1, 0) // width)
	return count

# Returns index of selection, or -1 if backspace was pressed
def ppmenu(provided_entries, titles='', startat=0, typer=False):
	if type(titles) is list:
		titles = [(n, t.strip()) for (n, t) in titles]
	else:
		titles = [(0, titles.strip())]
	full_entries = [str.join('\n', ['  ' + line for line in e.strip().split('\n')]) for e in provided_entries]
	full_entries = [(e[1],re.sub(r'\W+', '', e[1]).lower(),e[0]) for e in enumerate(full_entries)]
	entries = list(full_entries)
	title = ''
	for (n, t) in titles:
		if not n is 0:
			entry = entries[n - 1]
			entries[n - 1] = (entry[0] + '\n\n' + t, entry[1], entry[2])
		else:
			title = t
	titlelen = linecount(title)
	entrieslen = [linecount(e[0]) for e in entries]
	typerbuf = ""
	cursel = 0
	y = 0

	(width, height) = gettermsize()

	def update_entries():
		nonlocal y, entries, entrieslen, cursel
		ppclear()
		ppwrite(0, 0, title)
		y = titlelen
		for i in range(len(entries)):
			if y + entrieslen[i] > height:
				lines = entries[i][0].split('\n')
				for i in range(height - y):
					ppwrite(0, y + i, lines[i])
				break
			ppwrite(0, y, entries[i][0])
			y += entrieslen[i]

		cursel = 0
		y = titlelen
		ppwrite(0, y, '>')

	update_entries()

	# This is hacky and needs to be redesigned for aesthetics
	ppwrite(0, y, ' ')
	top = titlelen
	bottom = height - 1
	for i in range(startat):
		if y + entrieslen[cursel] + entrieslen[cursel + 1] > bottom:
			scrollamount = entrieslen[cursel + 1] - (bottom - (y + entrieslen[cursel] - 1))
			ppscroll(top, bottom, scrollamount)
			cursel += 1
			y = bottom - (entrieslen[cursel] - 1)
			ppwrite(0, y, entries[cursel][0])
		else:
			y += entrieslen[cursel]
			cursel += 1
	ppwrite(0, y, '>')
	
	c = ppgetch()
	while not (c == ENTER or (not typer and c == ord('o'))): # Enter
		top = titlelen
		bottom = height - 1
		oldy = y
		if c == ESC: # Escape
			return -1
		elif c == BKSP: # Backspace
			typerbuf = typerbuf[:-1]
			entries = [e for e in full_entries if e[1].startswith(typerbuf)]
			entrieslen = [linecount(e[0]) for e in entries]
			update_entries()
		elif typer and (type(c) is not tuple and c >= ord('a') and c <= ord('z')): # Can't use isalnum because arrow codes become unicode letters
			typerbuf += chr(c)
			entries = [e for e in entries if e[1].startswith(typerbuf)]
			entrieslen = [linecount(e[0]) for e in entries]
			update_entries()
		elif (c == DOWN or (not typer and c == ord('j'))) and cursel < len(entries) - 1:
			ppwrite(0, oldy, ' ')
			if y + entrieslen[cursel] + entrieslen[cursel + 1] > bottom:
				scrollamount = entrieslen[cursel + 1] - (bottom - (y + entrieslen[cursel] - 1))
				ppscroll(top, bottom, scrollamount)
				cursel += 1
				y = bottom - (entrieslen[cursel] - 1)
				ppwrite(0, y, entries[cursel][0])
			else:
				y += entrieslen[cursel]
				cursel += 1
			ppwrite(0, y, '>')
		elif (c == UP or (not typer and c == ord('k'))) and cursel > 0:
			ppwrite(0, oldy, ' ')
			if y - entrieslen[cursel - 1] < top:
				scrollamount = entrieslen[cursel - 1] - (y - top)
				ppscroll(top, bottom, -scrollamount)
				cursel -= 1
				y = top
				ppwrite(0, y, entries[cursel][0])
			else:
				cursel -= 1
				y -= entrieslen[cursel]
			ppwrite(0, y, '>')
		c = ppgetch()
	
	return entries[cursel][2]
