import re
import pickle
import urllib.request
from bs4 import BeautifulSoup, Tag

class Character:
	characters = {}
	
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.seiyuus = []
		self.animes = {} # anime:role
	
	@classmethod
	def add(cls, id, name):
		if id not in cls.characters:
			cls.characters[id] = Character(id, name)
		return cls.characters[id]

class Seiyuu:
	seiyuus = {}
	
	def __init__(self, id, name, lang):
		self.id = id
		self.name = name
		self.lang = lang
		self.characters = []
		
	@classmethod
	def add(cls, id, name, lang):
		if id not in cls.seiyuus:
			cls.seiyuus[id] = Seiyuu(id, name, lang)
		return cls.seiyuus[id]

class Anime:
	animes = {}
	
	def __init__(self, id, name, url):
		self.id = id
		self.name = name
		url_parsed = list(urllib.parse.urlparse(url))
		url_parsed[2] = urllib.parse.quote(url_parsed[2])
		self.url = urllib.parse.urlunparse(url_parsed)
		self.characters = []
		self.seiyuus = []
	
	@classmethod
	def add(cls, id, name, url):
		if id not in cls.animes:
			cls.animes[id] = Anime(id, name, url)
		return cls.animes[id]

class Data:
	def __init__(self):
		self.animes = {}
		self.characters = {}
		self.seiyuus = {}

def getAnime(limit = 0):
	url = 'http://myanimelist.net/animelist/Shijie'
	
	# Create soup
	soup = BeautifulSoup(urllib.request.urlopen(url).read())
	
	# From anime list, extract anime links
	animetitles = soup('a', 'animetitle')
	# Title is in animetitles[i].span.text
	# URL is in animetitles[i]['href']
	
	i = 0
	animes = []
	for entry in animetitles:
		name = entry.span.text
		url = entry['href']
		id = int(re.search(r'/(\d+)/', url).group(1))
		animes.append(Anime.add(id, name, url))
		i += 1
		if i == limit:
			break
	
	return animes

def getChars(url):
	soup = BeautifulSoup(urllib.request.urlopen(url).read())
	animeid = int(re.search(r'/(\d+)/', url).group(1))
	charData = []
	seiyuuData = []
	# From character page, extract stuff
	for table in soup('h2')[5].next_siblings:
		if not isinstance(table, Tag):
			break
		td = table('td')[1]
		name = td.a.text
		url = td.a['href']
		type = td.div.text
		id = int(re.search(r'/(\d+)/', url).group(1))
		char = Character.add(id, name)
		char.animes[Anime.animes[animeid]] = type
		
		seiyuu = None
		trs = table('td')[2].find_all('tr')[:-1]
		for tr in trs:
			name = tr.a.text
			url = tr.a['href']
			lang = tr.small.text
			id = int(re.search(r'/(\d+)/', url).group(1))
			if lang == "Japanese":
				seiyuu = Seiyuu.add(id, name, lang)
				if seiyuu is not None:
					if seiyuu not in char.seiyuus:
						char.seiyuus.append(seiyuu)
					if char not in seiyuu.characters:
						seiyuu.characters.append(char)
					if seiyuu not in seiyuuData:
						seiyuuData.append(seiyuu)
		charData.append(char)
	return (charData, seiyuuData)

def buildData():
	length = len(Anime.animes)
	cur = 0
	for anime in Anime.animes.values():
		cur += 1
		print(str(cur) + '/' + str(length) + '... ', end='')
		charData, seiyuuData = getChars(anime.url + "/characters")
		anime.characters = charData
		anime.seiyuus = seiyuuData
		print('DONE')

def saveData(filename):
	data = Data()
	data.animes = Anime.animes
	data.characters = Character.characters
	data.seiyuus = Seiyuu.seiyuus
	fp = open(filename, 'wb')
	pickle.dump(data, fp)
	fp.close()

def loadData(filename):
	fp = open(filename, 'rb')
	data = pickle.load(fp)
	fp.close()
	Anime.animes = data.animes
	Character.characters = data.characters
	Seiyuu.seiyuus = data.seiyuus
